<?php
	class A {
		public function f( string $a ) {
			if ( ! method_exists( $this , $a ) ) {
				throw new \Exception( 'error' ) ;
			}

			return $this->$a( ) ;
		}
	}