<?php
/**
CREATE TABLE `table` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'идентификатор',
  `title` varchar(200) NOT NULL COMMENT 'название',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=3072 DEFAULT CHARSET=utf8 COMMENT='Таблица';

INSERT IGNORE INTO `table`(
	`title`
)
SELECT
	sha1( uuid( ) ) AS `title`
FROM
	`mysql`.`user` AS `u1` ,
	`mysql`.`user` AS `u2` ,
	`mysql`.`user` AS `u3` ,
	`mysql`.`user` AS `u4` ,
	`mysql`.`user` AS `u5`
ORDER BY
	1 ASC ;
*/

	$dbname = 'task88' ;

	$dbh = new PDO( 'mysql:dbname=' . $dbname , 'root' , 'f2ox9erm' ) ;
?>
<table>
	<thead>
		<tr>
<?php
	$fields = [ ] ;

	$sth = $dbh->prepare( '
SHOW FULL FIELDS FROM `table` ;
	' ) ;
	$sth->execute( ) ;
	while ( $row = $sth->fetch( \PDO::FETCH_ASSOC ) ) {
		$fields[ $row[ 'Field' ] ] = $row[ 'Comment' ] ;
		?>
			<th><?=htmlspecialchars( $row[ 'Comment' ] )?></th>
		<?php
	}
	$sth->closeCursor( ) ;
?>
		</tr>
	</thead>
	<tbody>
	<?php
		$sth = $dbh->prepare( "
SELECT
	`t1`.*
FROM
	`table` AS `t1`
ORDER BY
	1 ASC ;
		" ) ;
		$sth->execute( ) ;

		while ( $row = $sth->fetch( \PDO::FETCH_ASSOC ) ) {
			?><tr><?php
			foreach ( $fields as $field => $comment ) {
				?><td><?=htmlspecialchars( $row[ $field ] )?></td><?php
			}
			?></tr><?php
		}

		$sth->closeCursor( ) ;
	?>
    <?php ?>
	</tbody>
</table>