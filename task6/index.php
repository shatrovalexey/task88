<?php
ini_set( 'display_errors' , E_ALL ) ;
/**
CREATE TABLE `table` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'идентификатор',
  `title` varchar(200) NOT NULL COMMENT 'название',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=3072 DEFAULT CHARSET=utf8 COMMENT='Таблица';

CREATE TABLE `table2` (
  `table_id` bigint unsigned NOT NULL COMMENT 'идентификатор `table`',
  `value` char(40) NOT NULL COMMENT 'значение',
  PRIMARY KEY (`table_id`,`value`),
  CONSTRAINT `fk_table` FOREIGN KEY (`table_id`) REFERENCES `table` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='таблица2';

INSERT IGNORE INTO `table`(
	`title`
)
SELECT
	sha1( uuid( ) ) AS `title`
FROM
	`mysql`.`user` AS `u1` ,
	`mysql`.`user` AS `u2` ,
	`mysql`.`user` AS `u3` ,
	`mysql`.`user` AS `u4` ,
	`mysql`.`user` AS `u5`
ORDER BY
	1 ASC ;

INSERT IGNORE INTO `table2`(
	`table_id` , `value`
)
SELECT
	`t1`.`id` AS `table_id` ,
	sha1( uuid( ) ) AS `value`
FROM
	`table` AS `t1` ,
	`table` AS `t2`
ORDER BY
	rand( ) ASC ;
*/
	$dbh = new PDO( 'mysql:dbname=task88' , 'root' , 'f2ox9erm' ) ;

	function fields( $table ) {
		global $dbh ;

		$result = [ ] ;

		$sth_fields = $dbh->prepare( "
SHOW FULL FIELDS FROM `{$table}` ;
		" ) ;
		$sth_fields->execute( ) ;
		while ( $row = $sth_fields->fetch( \PDO::FETCH_ASSOC ) ) {
			$result[ $row[ 'Field' ] ] = $row[ 'Comment' ] ;
		}
		$sth_fields->closeCursor( ) ;

		return $result ;
	}
?>
<table>
	<thead>
		<tr>
<?php
	$fields1 = fields( 'table' ) ;
	$fields2 = fields( 'table2' ) ;

	foreach ( $fields1 + $fields2 as $field => $comment ) {
		?><th><?=htmlspecialchars( $comment )?></th><?php
	}
?>
		</tr>
	</thead>
	<tbody>
	<?php
		$sth = $dbh->prepare( "
SELECT
	`t1`.* ,
	`t2`.*
FROM
	`table` AS `t1`

	INNER JOIN `table2` AS `t2` ON
	( `t2`.`table_id` = `t1`.`id` ) ;
ORDER BY
	1 ASC ;
		" ) ;
		$sth->execute( ) ;

		while ( $row = $sth->fetch( \PDO::FETCH_ASSOC ) ) {
			?><tr><?php
			foreach ( $fields1 + $fields2 as $field => $comment ) {
				?><td><?=htmlspecialchars( $row[ $field ] )?></td><?php
			}
			?></tr><?php
		}

		$sth->closeCursor( ) ;
	?>
	</tbody>
</table>