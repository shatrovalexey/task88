function array_flip( $array ) {
	let $result = [ ] ;

	for ( $key in $array ) {
		$result[ $array[ $key ] ] = $key ;
	}

	return $result ;
}