<?php
	header( 'Content-Type: application/json' ) ;

	$data = null ;

	try {
		if ( empty( $_FILES[ 'file' ] ) ) {
			throw new \Exception( 'Не указан файл' ) ;
		}
		if ( empty( $_FILES[ 'file' ][ 'tmp_name' ] ) ) {
			throw new \Exception( 'Почему-то нет временного имени' ) ;
		}
		if ( ! strlen( $data = file_get_contents( $_FILES[ 'file' ][ 'tmp_name' ] ) ) ) {
			throw new \Exception( 'Что-то не в порядке. Тонее, не удаётся прочитать загруженный файл. Может быть, нет прав на чтение.' ) ;
		}
		if ( ! ( $data = @json_decode( $data ) ) ) {
			throw new \Exception( 'Не могу (или компьютер не может) раскодировать файл, рассчитывая что это JSON.' ) ;
		}
	} catch ( \Exception $exception ) {
		die( json_encode( [
			'error' => $exception->getMessage( ) ,
		] ) ) ;
	}

	echo json_encode( $data ) ;